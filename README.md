# Custom Pause

A simple module for Foundry VTT that uses a file picker to change the Game Paused image.
![FVTT Icon](https://gitlab.com/jestevens210/custom-pause/-/raw/main/images/custom-pause-gif.gif)

## How to Use This Module

- Open Module Settings: Select the path to your desire image using the file picker and "Save Changes".
- Pause/Unpause: Use spacebar to Pause/Unpause and new image will appear.

## Releases

1.0.0 - Initial Release (June, 2021)
