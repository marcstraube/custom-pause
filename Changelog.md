# Custom Pause Foundry VTT Module Changelog

## 1.0.0 Initial release

- Ability to set custom pause icon for your game.

## 1.0.1 Initial release

- Fixed typo in module.json
